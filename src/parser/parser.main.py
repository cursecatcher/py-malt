#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys, os

from parser import Parser, ParsingError
from treebank import Treebank, tree
from evaluation import Evaluation

from timeit import default_timer as timer

def format_time(time):
    strtime = "{:03.5f} m".format(time/60) if time >= 60 else "{:03.5f} s".format(time)
    return strtime


if __name__ == "__main__":
    if len(sys.argv) < 4:
        sys.exit("Usage: ./{} training-set_path test-set_path result-file_path".format(sys.argv[0]))

    training_set = sys.argv[1]
#    dev_set = sys.argv[2]
    test_set = sys.argv[2]
    result_file = sys.argv[3]

    parser = Parser()

    try:
        print("Trying to load trained model from file...")
        parser.load_model("test_model")
    except FileNotFoundError:
        print("Training parser on {}... ".format(training_set), flush=True)
        start = timer()
        parser.fit_oracle(training_set)
        print("Training completed in {}".format(format_time(timer() - start)), flush=True)
        parser.save_model("test_model")

################################################################################

    # dev_set = test_set
    #
    # print("Test parser on {}".format(dev_set), flush=True)
    # tb = Treebank().parse(dev_set)
    # evaluation = Evaluation(num_classes=3)
    #
    # for index, (sentence, gold_tree) in enumerate(tb):
    #     try:
    #         predicted_tree = parser.parse(sentence)
    #         print(predicted_tree)
    #         evaluation.update(gold_tree, predicted_tree)
    #     except ParsingError:
    #         print("Can't parse sentence {}".format(index))
    #
    # print("\nExact match: {} ({}/{})".format(evaluation.exact_match, evaluation.num_exact_tree, evaluation.tot_tree))
    # print("Unlabelled attachment score: {}".format(evaluation.unlabelled_attachment_score))
    # print("Labelled attachment score: {}".format(evaluation.labelled_attachment_score))
    # print("Precision: {}".format(evaluation.get_precision()))
    # print("Recall: {}\n".format(evaluation.get_recall()))

################################################################################

    print("Testing parser on {}".format(test_set))

    predictions = Treebank()

    for index, sentence in enumerate(Treebank().parse(test_set, labelled=False)):
        try:
            predicted_tree = parser.parse(sentence)
#            print(predicted_tree.str())
        except ParsingError as pe:
            print("Can't parse sentence {}".format(index))
            predicted_tree = pe.tree

        predictions.add_sentence(predicted_tree)
    else:
        predictions.persist(result_file)
        print("Results wrote in {}".format(result_file))
